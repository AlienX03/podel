<?php 
      session_start();
      include('dbconfig.php');
      $orderNo = $_POST['order'];
      //$orderNo="Tester_1_JAN2020";
      

    //Fetching Values from ordernumber
    $buyerOrderQuery = "SELECT * FROM serverdb.buyerdb where orderno='".$orderNo."'";
    $buyerResults=$dbh->prepare($buyerOrderQuery);
    $buyerResults->execute();
    $buyerRow = $buyerResults->fetchAll(PDO::FETCH_ASSOC);

    //Fetching values from size quantity table
    $sqBuyerOrderQuery = "SELECT * FROM serverdb.sizequantitybuyer where orders='".$orderNo."'";
    $sqBuyerResults=$dbh->prepare($sqBuyerOrderQuery);
    $sizeArr = [];
    if($sqBuyerResults->execute()){
        while($sqBuyerRow = $sqBuyerResults->fetch(PDO::FETCH_ASSOC))  {
            array_push($sizeArr,$sqBuyerRow['size']);
          }
        }

?>
<!DOCTYPE html>
      <html lang="en">
        <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <head>
            
        </head>
        <body>
                  <div class="row">
                        <div class="col-sm-12">
                              <div class="col-sm-2">
                                    <div class="col-sm-6"><strong style="font-size: 22px;">Buyer Name:</strong></div>
                                    <div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $buyerRow[0]['name'];?></p></div>
                              </div>
                              <div class="col-sm-2">
                                    <div class="col-sm-6"><strong style="font-size: 22px;">Date:</strong></div>
                                    <div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $buyerRow[0]['date'];?></p></div>
                              </div>
                              <div class="col-sm-3">
                                    <div class="col-sm-6"><strong style="font-size: 22px;">Order No:</strong></div>
                                    <div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $orderNo;?></p></div>
                              </div>
                        </div>

                  </div>
                  <?php 


                        $nextedExecuter = "SELECT @rownum:=@rownum+1 No,style,colour,brand,image,design,quality,comment FROM serverdb.inputfields,(select @rownum:=0) r where orderNo='".$orderNo."'";
                            $nestedResults=$dbh->prepare($nextedExecuter);
                            $nestedResults->execute();
                            $orderNestedRow = $nestedResults->fetch(PDO::FETCH_ASSOC);
                            $sizeQuantityQuery = "select size,quantity from serverdb.sizequantity where orderNO='".$orderNo."'";
                            $sizeQuantityResult = $dbh->prepare($sizeQuantityQuery);
                            ?>
                        <hr>
<div class="row" style="padding-left: 50px;">
                  
                  <div class="row">
                  <div class="col-sm-12">
                        <div class="col-sm-1">
                              <strong>Style</strong>
                              <p ><?php echo $orderNestedRow ['style']; ?></p>
                              
                        </div>
                        <div class="col-sm-1">
                              <strong>Color</strong>
                              <p ><?php echo $orderNestedRow ['colour']; ?></p>
                              
                        </div>
                        <div class="col-sm-2">
                              <strong>Brand</strong>
                              <p ><?php echo $orderNestedRow ['brand']; ?></p>
                              
                        </div>
                        <div class="col-sm-1">
                              <div class="row"><strong>Image</strong></div>
                              <div class="row"><img src="<?php echo $orderNestedRow ['image'];?>" alt='Red Dot' height='64' width='64' border='1'/></div>
                              
                        </div>
                        <div class="col-sm-2">
                              <strong>Design</strong>
                              <p ><?php echo $orderNestedRow ['design']; ?></p>
                              
                        </div>
                        <div class="col-sm-2">
                              <strong>Quality</strong>
                              <p ><?php echo $orderNestedRow ['quality']; ?></p>
                              
                        </div>
                        <div class="col-sm-1">
                              <strong>Comment</strong>
                              <p ><?php echo $orderNestedRow ['comment']; ?></p>
                              
                        </div>
                        

                  </div>
                  <div class="row">
                    <div class="col-xs-5">
                      <div class="row">
                        <div class="col-xs-5">
                              <STRONG>Ordered Size</STRONG>
                        </div>
                        <div class="col-xs-5">
                              <STRONG>Ordered Quantity</STRONG>
                        </div>
                        </div>
                      
                  
                  <?php if($sizeQuantityResult->execute()){
                        while($sqRow = $sizeQuantityResult->fetch(PDO::FETCH_ASSOC))  { ?>
                              <div class = "row">
                                    <div class="col-xs-5">
                                          <p><?php echo $sqRow['size'];?></p>
                                          
                                    </div>
                                    <div class="col-xs-5">
                                          <p><?php echo $sqRow['quantity'];?></p>
                                          
                                    </div>

                              </div>

                  
                        
                  <?php }} ?>
                  </div>
                  <div class="col-xs-5">
                        <div class="row">
                        <div class="col-xs-5">
                              <STRONG>Delivered Size</STRONG>
                        </div>
                        <div class="col-xs-5">
                              <STRONG>Delivered Quantity</STRONG>
                        </div>

                        <?php  if($_SESSION['type']==1){?>
                                    <div class="col-sm-1">
                                      <div class="row"><strong>Edit</strong></div>
                                      <div class="row"><button class="btn btn-xs btn-primary" onclick="edit()"><span class="glyphicon glyphicon-pencil"></span></button></div>
                                      
                                    </div>
                                    <div class="col-sm-1">
                                      <div class="row"><strong>Update</strong></div>
                                      <div class="row"><button class="btn btn-xs btn-primary" onclick="update()"><span class="glyphicon glyphicon-ok"></span></button></div>
                                      
                                    </div>


              <?php }?>
              </div>

                        
                  
                      <?php if($sqBuyerResults->execute()){
                        while($sqBuyerRow = $sqBuyerResults->fetch(PDO::FETCH_ASSOC))  { ?>
                              <div class = "row">
                                    <div class="col-xs-5">
                                          <p><?php echo $sqBuyerRow['size'];?></p>
                                          
                                    </div>
                                    <div class="col-xs-5">
                                          <p><?php echo $sqBuyerRow['quantity'];?></p>
                                          <quantity></quantity>
                                    </div>
                                    
                              </div>

                  
                        
                  <?php }} ?>
</div>
                              </div>
                             
            
        </div>



                  


            </div>
            <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <script>
          window.edit = function(){
            console.log("Defined");
            var quantity = '<input type="text" class="quantity col-sm-12" id="quantity" style="box-sizing: border-box;"></input>';
            $('quantity').append(quantity);
          }

          window.update = function(){
                    var check=0;
                    iterator=0;
                   
                    var quantity=document.getElementsByClassName('quantity');
                    var order = "<?php echo $orderNo;?>";
                    var quantityArr = [];
                    
                    var quantityIterator;
                    for(quantityIterator=0;quantityIterator<quantity.length;quantityIterator++){
                      if(quantity.item(quantityIterator).value==''){
                        quantityArr.push(-1); 
                      }
                      else{
                        quantityArr.push(quantity.item(quantityIterator).value);  
                      
                      }
                      
                    }
                    
                    var jsonQuantity = JSON.stringify(quantityArr);
                    var tempsizeArray= '<?php echo json_encode($sizeArr)?>'
                    var temp=tempsizeArray.replace(/","/g,",");
                    var temp2=temp.replace('["',"");
                    var temp3=temp2.replace('"]',"");
                    var sizeArrs = temp3.split(",");
                    var jsonSize = JSON.stringify(sizeArrs);
                    //uPDATING SIZE AND QUANTITY VALUES TO THE TABLE
                    var form = new FormData();
                    form.append("quantity",jsonQuantity);
                    form.append("size",jsonSize);
                    form.append("order",order);
                    form.append("action","edit");
                    $.ajax({
                      url: "buyerActionSQ.php",
                  type: "POST",
                  data: form,
                  processData: false,
                  contentType: false,
                  success: function(response) {
                    if(response.includes("success")){
                      alert("success");
                    }
                    else{
                      alert("failed");
                    }
                  },
                  error: function(jqXHR, textStatus, errorMessage) {
                      console.log(errorMessage); // Optional
                  }
                    });

                    
                   
                
                  
                  
                    $('quantity').remove();
                  }
        </script>
        </body>

        </html>
