<?php
include('dbconfig.php');
session_start();


try {
	$mailQueryExecuter = "SELECT * FROM serverdb.adminmail;";
    $mailResults=$dbh->prepare($mailQueryExecuter);
}
catch(Exception $e){
	print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>PODEL</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
	<style> 
            
            .gfg { 
                border-collapse:separate; 
                border-spacing:0 15px; 
            } 
            
        </style> 
  </head>

  <body class="nav-md">
  
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="indexView.php" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <div id="name" class="text-uppercase">
				</div>
              </div>
            </div>
            <!-- /menu profile quick info -->

            

            <!-- sidebar menu -->
           <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                      <li><a href="indexView.php">Dashboard</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a href="tables.php">Purchase Order</a></li>
                      <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                    <ul class="nav child_menu">
                      <li><a href="settings.php">Business Info</a></li>
                      <li><a href="vendor.php">Vendors</a></li>
                      <li><a href="buyer.php">Buyers</a></li>
                      <li><a href="size.php">Size</a></li>
                    </ul>
                  </li>
                      <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
					   <ul class="nav child_menu">
                      <li><a href="POsVendor.php">All POs</a></li>
                      <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                      <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>
                      
					  </li>
                    </ul>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-large ">
                          
                          <a data-toggle="tooltip" data-placement="top" title="Logout" href="loginView.php">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                          </a>
                        </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
		<!--pageContent -->
		     
	<div class="right_col" role="main">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
					<form class="form-horizontal form-label">

						<div class="row form-group pull-center col-sm-12">
              <div class='col-sm-4 '><p style='font-size: 20px;'>Business Info</p></div>
            </div>
            <div class="row form-group col-sm-12">
              <div class='col-sm-4 pull-left'><p style='font-size: 20px;'>Business Name:</p></div>
            </div>
            <div class="row form-group col-sm-12">
              <div class='col-sm-4 pull-left'><p style='font-size: 20px;'>Address:</p></div>
            </div>
            <?php if($mailResults->execute()){
              while($mailRow = $mailResults->fetch(PDO::FETCH_ASSOC)){ ?>
                <div class="row form-group col-sm-12">
                  <div class='col-sm-4 pull-left'><p style='font-size: 20px;'>Mail ID:</p></div>
                  <div class='col-sm-4 pull-left'><p style='font-size: 20px;'><?php echo $mailRow['mail'];?></p></div>
                </div>
             <?php }} ?>
             <whatsapp>
               
             </whatsapp>
            
            <div class='row form-group col-sm-12 pull-left'>
             <email>
               
             </email>
            </div>
            <div class="pull-right">
              <button type="button" class="btn btn-primary btn-lg" onclick="Create()">Update </button>
            </div>
					<div class="row form-group pull-right">
              <button type="button" class="btn btn-primary btn-lg" onclick="AddWhatsappNo()">
<span class="glyphicon glyphicon-phone" style="font-size: 20px;"></span> <small>AddWhatsappNo</small>
              </button>
            </div> 
            <div class="row form-group pull-right">
              <button type="button" class="btn btn-primary btn-lg" onclick="AddEmail()">
                <span class="glyphicon glyphicon-envelope" style="font-size: 20px;"></span> <small>AddEmail</small>
              </button>
            </div> 

					</form>
				</div>
			</div>
		</div>
          
                
          
	</div>
					<!-- /page content -->

<script>
  var whatsappCount=0;
  var emailCount=0;
  window.AddWhatsappNo = function(){

    var whatsapp = "";
    whatsapp="<div class='col-sm-12'><div class='col-sm-4 pull-left'><p style='font-size: 20px;'>whatsapp:</p></div><div class='col-sm-8'><input type='text' id='whatsapp"+whatsappCount+"' style='font-size: 20px;' class='form-control'></input></div></div>";
    $('whatsapp').append(whatsapp);
    whatsappCount++;
  }
   window.AddEmail = function(){
    var email = "";
    email="<div class='col-sm-12'><div class='col-sm-4 pull-left'><p style='font-size: 20px;'>email:</p></div><div class='col-sm-8'><input type='text' id='email"+emailCount+"' style='font-size: 20px;' class='form-control'></input></div></div>";
    $('email').append(email);
    emailCount++;
   }
   window.Create = function(){
      var whatsappArr = [];
      var emailArr = [];
      var iterator1;
      var iterator2;
      for(iterator1=0;iterator1<whatsappCount;iterator1++){
        var temp=document.getElementById('whatsapp'+iterator1);
        whatsappArr.push(temp.value);
      }
      for(iterator2=0;iterator2<emailCount;iterator2++){
        var temp2=document.getElementById('email'+iterator2);
        emailArr.push(temp2.value);
      }
      
      var jsonWhatsapp = JSON.stringify(whatsappArr);
      var mailId = JSON.stringify(emailArr);
      var formdata = new FormData();
      formdata.append("whatsapp",jsonWhatsapp);
      formdata.append("mailid",mailId);
      $.ajax({
       url: "updateAdmin.php",
       type: "POST",
       data: formdata,
       processData: false,
       contentType: false,
       success: function(response) {
       console.log(response);
       },
       error: function(jqXHR, textStatus, errorMessage) {
           console.log(errorMessage); // Optional
       }
    });

   }
</script>
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
	<script src="../vendors/moment/min/moment.min.js"></script>
	<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<script type="text/javascript" src="js/settings.js"></script>
    <script src="../build/js/custom.min.js"></script>
	<script src="js/bootstable.js"></script>
<script type="text/javascript" src="js/profileEditable.js"></script>
<script type="text/javascript" src="js/whatsappAdd.js"></script>
	</body>
         
