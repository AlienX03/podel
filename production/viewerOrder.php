<?php 
	session_start();
	include('dbconfig.php');
	$order = $_POST['order'];
	$queryExecuter = "SELECT @rownum:=@rownum+1 No,style,colour,brand,image,design,quality,comment FROM serverdb.inputfields,(select @rownum:=0) r where orderNo='".$order."'";
    $results=$dbh->prepare($queryExecuter);
    $results->execute();
    $row = $results->fetch(PDO::FETCH_ASSOC);
    $sizeQuantityQuery = "select size,quantity from serverdb.sizequantity where orderNO='".$order."'";
    $sizeQuantityResult = $dbh->prepare($sizeQuantityQuery);
    
?>
<!DOCTYPE html>
	<html lang="en">
	  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	  <head>
	  	
	  </head>
	  <body>
	  	<div class="row" style="padding-left: 50px;">
	  		<strong>Order No</strong>
	  		<div id="order"><?php echo $order;?></div>
	  	</div>
	  	<hr>
	  	<div class="row" style="padding-left: 50px;">
	  		
	  		<div class="row">
            	<div class="col-sm-12">
            		<div class="col-sm-1">
            			<strong>Style</strong>
            			<p ><?php echo $row ['style']; ?></p>
            			<styles></styles>
            		</div>
            		<div class="col-sm-1">
            			<strong>Color</strong>
            			<p ><?php echo $row ['colour']; ?></p>
            			<color></color>
            		</div>
            		<div class="col-sm-2">
            			<strong>Brand</strong>
            			<p ><?php echo $row ['brand']; ?></p>
            			<brand></brand>
            		</div>
            		<div class="col-sm-1">
            			<div class="row"><strong>Image</strong></div>
            			<div class="row"><img src="<?php echo $row ['image'];?>" alt='Red Dot' height='64' width='64' border='1'/></div>
            			
            		</div>
            		<div class="col-sm-2">
            			<strong>Design</strong>
            			<p ><?php echo $row ['design']; ?></p>
            			<design></design>
            		</div>
            		<div class="col-sm-2">
            			<strong>Quality</strong>
            			<p ><?php echo $row ['quality']; ?></p>
            			<quality></quality>
            		</div>
            		<div class="col-sm-1">
            			<strong>Comment</strong>
            			<p ><?php echo $row ['comment']; ?></p>
            			<comment></comment>
            		</div>
            		<?php  if($_SESSION['type']==1){?>
            		<div class="col-sm-1">
            			<div class="row"><strong>Edit</strong></div>
            			<div class="row"><button class="btn btn-xs btn-primary" onclick="edit()"><span class="glyphicon glyphicon-pencil"></span></button></div>
            			
            		</div>
            		<div class="col-sm-1">
            			<div class="row"><strong>Update</strong></div>
            			<div class="row"><button class="btn btn-xs btn-primary" onclick="update()"><span class="glyphicon glyphicon-ok"></span></button></div>
            			
            		</div>

            	<?php }?>

            	</div>
            	<div class="row">
            		<div class="col-sm-2">
            			<STRONG>Size</STRONG>
            		</div>
            		<div class="col-sm-2">
            			<STRONG>Quantity</STRONG>
            		</div>
            	</div>
            	<?php if($sizeQuantityResult->execute()){
            		while($sqRow = $sizeQuantityResult->fetch(PDO::FETCH_ASSOC))  { ?>
            			<div class = "row">
            				<div class="col-sm-2">
            					<p><?php echo $sqRow['size'];?></p>
            					<size></size>
            				</div>
            				<div class="col-sm-2">
            					<p><?php echo $sqRow['quantity'];?></p>
            					<quantity></quantity>
            				</div>

            			</div>

            	
            		
            	<?php }} ?>
	                        
	                       
            </div>
        </div>
                <script src="../vendors/jquery/dist/jquery.min.js"></script>
                <script >
                	var iterator=0;
                	window.edit = function(){
                		
                		if(iterator==0){
	                		var style='<input type="text" class="col-sm-12" id="style" style="box-sizing: border-box;"></input>';
	                		var color='<input type="text" class="col-sm-12" id="color" style="box-sizing: border-box;"></input>';
	                		var brand='<input type="text" class="col-sm-12" id="brand" style="box-sizing: border-box;"></input>';
	                		var design='<input type="text" class="col-sm-12" id="design" style="box-sizing: border-box;"></input>';
	                		var quality='<input type="text" class="col-sm-12" id="quality" style="box-sizing: border-box;"></input>';
	                		var comment='<input type="text" class="col-sm-12" id="comment" style="box-sizing: border-box;"></input>';
	                		var size = '<input type="text" class="size col-sm-12" id="size" style="box-sizing: border-box;"></input>';
	                		var quantity = '<input type="text" class="quantity col-sm-12" id="quantity" style="box-sizing: border-box;"></input>';
	                		$('styles').append(style);
	                		$('color').append(color);
	                		$('brand').append(brand);
	                		$('design').append(design);
	                		$('quality').append(quality);
	                		$('comment').append(comment);
	                		$('size').append(size);
	                		$('quantity').append(quantity);
	                		iterator++;
                		}
                	}
                	window.update = function(){
                		var check=0;
                		iterator=0;
                		var style=document.getElementById('style');
                		var color=document.getElementById('color');
                		var brand=document.getElementById('brand');
                		var design=document.getElementById('design');
                		var quality=document.getElementById('quality');
                		var comment=document.getElementById('comment');
                		var size = document.getElementsByClassName('size');
                		var quantity=document.getElementsByClassName('quantity');
                		var order = "<?php echo $order;?>";
                		var quantityArr = [];
                		var sizeArr = [];
                		var quantityIterator,sizeIterator;
                		for(quantityIterator=0;quantityIterator<quantity.length;quantityIterator++){
                			if(quantity.item(quantityIterator).value==''){
                				quantityArr.push(-1);	
                			}
                			else{
                				quantityArr.push(quantity.item(quantityIterator).value);	
                			
                			}
                			
                		}
                		for(sizeIterator=0;sizeIterator<size.length;sizeIterator++){
                			if(size.item(sizeIterator).value==''){
                				sizeArr.push(-1);	
                			}
                			else{
                				sizeArr.push(size.item(sizeIterator).value);	
                			
                			}
                			
                		}
                		var jsonQuantity = JSON.stringify(quantityArr);
                		var jsonSize = JSON.stringify(sizeArr);
                		//uPDATING SIZE AND QUANTITY VALUES TO THE TABLE
                		var form = new FormData();
                		form.append("quantity",jsonQuantity);
                		form.append("size",jsonSize);
                		form.append("order",order);
                		form.append("action","edit");
                		$.ajax({
                			url: "actionsq.php",
					        type: "POST",
					        data: form,
					        processData: false,
					        contentType: false,
					        success: function(response) {
					        	if(response.includes("success")){
					        		alert("success");
					        	}
					        },
					        error: function(jqXHR, textStatus, errorMessage) {
					            console.log(errorMessage); // Optional
					        }
                		});

                		
                		if(style.value==""){
                			style=0;
                		}
                		else{
                			style=style.value;
                		}
                		if(color.value==""){
                			color=0;
                		}
                		else{
                			color=color.value;
                		}
                		if(brand.value==""){
                			brand=0;
                		}
                		else{
                			brand=brand.value;
                		}
                		if(design.value==""){
                			design=0;
                		}
                		else{
                			design=design.value;
                		}
                		if(quality.value==""){
                			quality=0;
                		}
                		else{
                			quality=quality.value;
                		}
                		if(comment.value==""){
                			comment=0;
                		}
                		else{
                			comment=comment.value;
                		}
                		
                		var formData = new FormData();
                		formData.append("style",style);
                		formData.append("color",color);
                		formData.append("brand",brand);
                		formData.append("design",design);
                		formData.append("quality",quality);
                		formData.append("comment",comment);
                		formData.append("action","edit");
                		formData.append("order",order);
                		$.ajax({
					         url: "action.php",
					         type: "POST",
					         data: formData,
					         processData: false,
					         contentType: false,
					         success: function(response) {
					         	if(response.includes("success")){
					         		check=1;
					         		alert("success");
					         		window.close();
					         	}
					         	
					         },
					         error: function(jqXHR, textStatus, errorMessage) {
					             console.log(errorMessage); // Optional
					         }
					      });
        				
        					
        					//alert("Sorry values are not updated, please try again");
        					
        				
                		$('styles').remove();
	                	$('color').remove();
	                	$('brand').remove();
	                	$('design').remove();
	                	$('quality').remove();
	                	$('comment').remove();
	                	$('size').remove();
	                	$('quantity').remove();
                	}
                </script>

            
	  </body>
	  
	  </html>
