<?php 
	session_start();
	include('dbconfig.php');
	$POno = $_POST['POno'];
	//$POno = 'Ragul_2_JAN2020';
	//Fetching id from po number
	$vendorIdQuery = "SELECT vid FROM serverdb.finalpo where POno = ".$POno."";
    $vendorIdPrepare = $dbh->prepare($vendorIdQuery);
    $vendorIdPrepare->execute();
    $vendorIdresult=$vendorIdPrepare->fetchAll(PDO::FETCH_ASSOC);
    $vendorId = $vendorIdresult[0]['vid'];
    
    //fetching buyer and vendor name from vendor id
    $purchaseOrderQuery = "SELECT vendor,buyer FROM serverdb.vendordb where id='".$vendorId."';";
    $results=$dbh->prepare($purchaseOrderQuery);
    $results->execute();
    $row = $results->fetch(PDO::FETCH_ASSOC);

    //fetching dates from purchase order number
    $datesQuery = "SELECT date,duedate FROM serverdb.vendordateduedate where POno =".$POno." limit 1";
    $datesResults=$dbh->prepare($datesQuery);
    $datesResults->execute();
    $dateRow = $datesResults->fetch(PDO::FETCH_ASSOC);
    

    $orderNumber = [];
    //Fetching corresponding order numbers from table
    $orderQuery = "SELECT * FROM serverdb.finalorderno where POno = ".$POno."";
    $ordersResults = $dbh->prepare($orderQuery);

    if($ordersResults->execute()){
    	while($orderRow = $ordersResults->fetch(PDO::FETCH_ASSOC)){
    		array_push($orderNumber,$orderRow['orders']);
    	}
    }

?>
<!DOCTYPE html>
	<html lang="en">
	  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	  <head>
	  	
	  </head>
	  <body>
	  		<div class="row">
	  			<div class="col-sm-12">
	  				<div class="col-sm-2">
	  					<div class="col-sm-6"><strong style="font-size: 22px;">Vendor Name:</strong></div>
	  					<div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $row['vendor'];?></p></div>
	  				</div>
	  				<div class="col-sm-2">
	  					<div class="col-sm-6"><strong style="font-size: 22px;">Buyer Name:</strong></div>
	  					<div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $row['buyer'];?></p></div>
	  				</div>
	  				<div class="col-sm-2">
	  					<div class="col-sm-6"><strong style="font-size: 22px;">Date:</strong></div>
	  					<div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $dateRow['date'];?></p></div>
	  				</div>
	  				<div class="col-sm-3">
	  					<div class="col-sm-6"><strong style="font-size: 22px;">DueDate :</strong></div>
	  					<div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $dateRow['duedate'];?></p></div>
	  				</div>
	  				<div class="col-sm-3">
	  					<div class="col-sm-6"><strong style="font-size: 22px;">PurchaseOrder No:</strong></div>
	  					<div class="col-sm-6 pull-left"><p style="font-size: 20px;"><?php echo $POno;?></p></div>
	  				</div>
	  			</div>

	  		</div>
            	<?php foreach ($orderNumber as $key => $value) { 


            		$nextedExecuter = "SELECT @rownum:=@rownum+1 No,style,colour,brand,image,design,quality,comment FROM serverdb.inputfields,(select @rownum:=0) r where orderNo='".$value."'";
				    $nestedResults=$dbh->prepare($nextedExecuter);
				    $nestedResults->execute();
				    $orderNestedRow = $nestedResults->fetch(PDO::FETCH_ASSOC);
				    $sizeQuantityQuery = "select size,quantity from serverdb.sizequantity where orderNO='".$value."'";
				    $sizeQuantityResult = $dbh->prepare($sizeQuantityQuery);
				    ?>
            		<hr>
<div class="row" style="padding-left: 50px;">
	  		
	  		<div class="row">
            	<div class="col-sm-12">
            		<div class="col-sm-1">
            			<strong>Style</strong>
            			<p ><?php echo $orderNestedRow ['style']; ?></p>
            			<styles></styles>
            		</div>
            		<div class="col-sm-1">
            			<strong>Color</strong>
            			<p ><?php echo $orderNestedRow ['colour']; ?></p>
            			<color></color>
            		</div>
            		<div class="col-sm-2">
            			<strong>Brand</strong>
            			<p ><?php echo $orderNestedRow ['brand']; ?></p>
            			<brand></brand>
            		</div>
            		<div class="col-sm-1">
            			<div class="row"><strong>Image</strong></div>
            			<div class="row"><img src="<?php echo $orderNestedRow ['image'];?>" alt='Red Dot' height='64' width='64' border='1'/></div>
            			
            		</div>
            		<div class="col-sm-2">
            			<strong>Design</strong>
            			<p ><?php echo $orderNestedRow ['design']; ?></p>
            			<design></design>
            		</div>
            		<div class="col-sm-2">
            			<strong>Quality</strong>
            			<p ><?php echo $orderNestedRow ['quality']; ?></p>
            			<quality></quality>
            		</div>
            		<div class="col-sm-1">
            			<strong>Comment</strong>
            			<p ><?php echo $orderNestedRow ['comment']; ?></p>
            			<comment></comment>
            		</div>
            		

            	</div>
            	<div class="row">
            		<div class="col-sm-2">
            			<STRONG>Size</STRONG>
            		</div>
            		<div class="col-sm-2">
            			<STRONG>Quantity</STRONG>
            		</div>
            	</div>
            	<?php if($sizeQuantityResult->execute()){
            		while($sqRow = $sizeQuantityResult->fetch(PDO::FETCH_ASSOC))  { ?>
            			<div class = "row">
            				<div class="col-sm-2">
            					<p><?php echo $sqRow['size'];?></p>
            					<size></size>
            				</div>
            				<div class="col-sm-2">
            					<p><?php echo $sqRow['quantity'];?></p>
            					<quantity></quantity>
            				</div>

            			</div>

            	
            		
            	<?php }} ?>
	                        
	                       
            </div>
        </div>



            	<?php } ?>


            </div>
	  </body>
	  </html>
