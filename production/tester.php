<?php


function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}


require_once __DIR__ . '/vendor/autoload.php';

$htmlCode=$_POST['html'];
$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);


ini_set("pcre.backtrack_limit", "5000000");
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0; 

$stylesheet= file_get_contents('CustomCSS/bootstrap.min.css');

$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);

$mpdf->WriteHTML($htmlCode, \Mpdf\HTMLParserMode::HTML_BODY);

$filename = "tempLogs/".random_string(10).".pdf";
$mpdf->output($filename,'F');
echo $filename;

?>