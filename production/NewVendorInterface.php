<?php
session_start();
include 'dbconfig.php';
if(isset($_SESSION['uid'])){


  $sizeQueryExecuter = "SELECT * FROM serverdb.size;";
  $sizeResults=$dbh->prepare($sizeQueryExecuter);
  $list = [];
  if ($sizeResults->execute()) {
    while ($sizeRow = $sizeResults->fetch(PDO::FETCH_ASSOC)) {
        array_push($list, $sizeRow['size']);
    }
  }
  $vendorName = "SELECT id,name FROM serverdb.vendorinfo;";
      $vendorResults=$dbh->prepare($vendorName);
      $vendorNames = [];
      if ($vendorResults->execute()) {
        while ($vendorRow = $vendorResults->fetch(PDO::FETCH_ASSOC)) {
            array_push($vendorNames, $vendorRow['name']);
        }
      }
  $buyerName = "SELECT id,name FROM serverdb.buyerinfo;";
      $buyerResults=$dbh->prepare($buyerName);
      $buyerNames = [];
      if ($buyerResults->execute()) {
        while ($buyerRow = $buyerResults->fetch(PDO::FETCH_ASSOC)) {
            array_push($buyerNames, $buyerRow['name']);
        }
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>PODEL</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="indexView.php" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <div id="name" class="text-uppercase">
				</div>
              </div>
            </div>
            <!-- /menu profile quick info -->

            

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                      <li><a href="indexView.php">Dashboard</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a href="tables.php">Purchase Order</a></li>
                      <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                    </ul>
                    <?php if($_SESSION['type']==1) { ?>
                                <ul class="nav side-menu">
                                  <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                                <ul class="nav child_menu">
                                  <li><a href="settings.php">Business Info</a></li>
                                  <li><a href="vendor.php">Vendors</a></li>
                                  <li><a href="buyer.php">Buyers</a></li>
                                  <li><a href="size.php">Size</a></li>
                                </ul>
                              </li>
                                  <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
                         <ul class="nav child_menu">
                                  <li><a href="POsVendor.php">All POs</a></li>
                                  <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                                  <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>

                        </li>
                                </ul>
                              <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-large ">
                          
                          <a data-toggle="tooltip" data-placement="top" title="Logout" href="loginView.php">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                          </a>
                        </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              
            </nav>
          </div>
        </div>


        
        <!-- /top navigation -->
         

             
        <!-- /page content -->
	<div class="right_col" role="main">
    <div id="MyOwnClass">
      <mailer>
        <removeContent>
          <rags>
            <form class="userBlock">
              <div class="">
                <div class="page-title">                
					         <form class="user">
					             <div class="form-group row">
					               <div class="col-lg-12  form-group top_search">
							            <div class="col-sm-2">	
                            <div class="input-group">
                              <div class="form-group" ><select class="form-control" id="vendor"></select></div>
                            </div>
							            </div>
                          <div class="col-sm-2">
                            <div class="input-group">
                              <div class="form-group" ><select class="form-control" id="buyer"></select></div>
                            </div>
                          </div>
            							<div class='col-sm-2'>
            								
            								<div class="form-group">
            									<div class='input-group date' id='myDatepicker2'>
            										<input type='text' class="form-control"  placeholder="Date" id="date"/>
            										<span class="input-group-addon">
            										<span class="glyphicon glyphicon-calendar"></span>
            										</span>
            									</div>
            								</div>
            							</div>
            							<div class='col-sm-2'>
            								
            								<div class="form-group">
            									<div class='input-group date' id='myDatepicker3'>
            										<input type='text' class="form-control"  placeholder="DueDate" id="duedate"/>
            										<span class="input-group-addon">
            										<span class="glyphicon glyphicon-calendar"></span>
            										</span>
            									</div>
            								</div>
            							</div>
							
							           <div class="col-sm-4 ">
                          <functionalities>
                            
                          </functionalities>
						
					</div>
								                													
					</form>
				   
<span class="border border-success">

<div class="container">
    <table id="myTable" class=" table order-list">
	
    <thead>
        <tr>
            <th width="11%">Style</th>
			<th width="18%">Fabric Colour</th>
            <th width="10%">Brand</th>
            <th width="11%">Image</th>
			<th width="10%">Design Name</th>
			
			<th width="9%">Quality </th>
			<th width="9%">Comments</th>
			
        </tr>
    </thead>
    <tbody>
        <tr>
            <td >
                <input type="text" name="name" class="form-control" id="style"/>
            </td>
            <td >
                <div class="form-group">
                        
                        <div class="col-sm-12">
                          <div class="input-group demo2">
                            <input type="text" value="#e01ab5" class="form-control" id="color"/>
                            <span class="input-group-addon"><i></i></span>
                          </div>
                        </div>
                      </div>
            </td>
            <td >
                <input type="text" name="Brand"  class="form-control" id="Brand"/>
            </td>
			<td >
				<span class="btn btn-default btn-file">
				<input type="file" id="images">
				</span>
				
            </td>
			<td >
                <input type="text"   class="form-control" id="DesignName"/>
            </td>
			
			<td >
                <input type="text" name="phone"  class="form-control" id="QualityInstructions"/>
            </td>
			<td >
                <input type="text" name="phone"  class="form-control" id="comments"/>
            </td>
            <td class="col-sm-2"><a class="deleteRow"></a>

            </td>
        </tr>
    </tbody>
    
</table>
</div>

<div class="container">
  <table id="myTableda" class=" table order-list">
    <thead>
      <tr>
            <td>Size</td>
            <td>Quantity</td>
            <td> <p>
        <input type="button" class="btn btn-primary btn-xl"value="Add" id="addnewrow" onclick="myFunction(-2)">
          
        </input>
      </p>
            </td>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
    <tfoot>
       
        
    </tfoot>
</table>
</div>
	      

      
  </span>
</div>
</div>
</form>
</div>
</div>
</form>
</rags>

</rags>

 </removeContent>
   </mailer>
   </div>
<create>
  <div class="col-sm-2">
  <input type="button" class="btn btn-sm btn-block btn-primary pull-left " id="addrow" value="Create" onclick="fetchValues()" />    
</div>

<div class="col-sm-2">
  <input type="button" class="btn btn-sm btn-block btn-primary pull-left " id="addnew" value="add new"  onClick="addNewBlock()" />   
  
</div>
</create> 
	
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
	<script src="../vendors/moment/min/moment.min.js"></script>
	<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    
<script type="text/javascript" src="js/newColumn.js"></script>

<script src="js/html2canvas.js"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script>
      //Vendor Names
      var vendorNameArr= '<?php echo json_encode($vendorNames);?>';
      var tempVendor=vendorNameArr.replace(/","/g,",");
      var tempVendor2=tempVendor.replace('["',"");
      var tempVendor3=tempVendor2.replace('"]',"");
      VendorLists = tempVendor3.split(",");
      var dynamicVendorId="vendor";
      var xVendor = document.getElementById(dynamicVendorId);
      var vendorOptions = document.createElement("option");
      vendorOptions.text = "Select vendor name";
      vendorOptions.value=0;
      xVendor.add(vendorOptions);
      for (var i = 0; i < VendorLists.length; i++) {                    
        var vendorOption = document.createElement("option");
        
        vendorOption.text = VendorLists[i];
        vendorOption.value=i+1;
        xVendor.add(vendorOption);
      }

      //Buyer details
      var buyerNameArr= '<?php echo json_encode($buyerNames);?>';
      var tempbuyer=buyerNameArr.replace(/","/g,",");
      var tempbuyer2=tempbuyer.replace('["',"");
      var tempbuyer3=tempbuyer2.replace('"]',"");
      buyerLists = tempbuyer3.split(",");
      var dynamicbuyerId="buyer";
      var xbuyer = document.getElementById(dynamicbuyerId);
      var buyerOptions = document.createElement("option");
      buyerOptions.text = "Select buyer name";
      buyerOptions.value=0;
      xbuyer.add(buyerOptions);
      for (var i = 0; i < buyerLists.length; i++) {                    
        var buyerOption = document.createElement("option");
        
        buyerOption.text = buyerLists[i];
        buyerOption.value=i+1;
        xbuyer.add(buyerOption);
      }

      var counterSizeQuantity=-1;
      var counterNewRow=-1;
      var rowCount = new Map();
      var sizeQuantityTemp;
      var PONO,Lists;


      function printFunction(){
       var formPdf = new FormData();
       var htmlCode;
       if(PONO!=undefined){
       formPdf.append("POno",PONO);
        
          $.ajax({
       url: "tableViewer.php",
       type: "POST",
       data: formPdf,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
        htmlCode=response;
          
       },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     }); 

          
      var formPdf1 = new FormData();
        formPdf1.append("html",htmlCode);
         $.ajax({
       url: "tester.php",
       type: "POST",
       data: formPdf1,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
         popupWindow = window.open("",'popUpWindow','height=3000,width=4000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
         popupWindow.location.href=response;
              },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });    
        }
      }
      function mailer(){
        var formPdf = new FormData();
       var htmlCode;
        var reciever = document.getElementById('mailId');
        if(reciever.value!=undefined){
        formPdf.append("POno",PONO);
        
          $.ajax({
       url: "tableViewer.php",
       type: "POST",
       data: formPdf,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
        htmlCode=response;
          
       },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     }); 

          
      var formPdf1 = new FormData();
        formPdf1.append("html",htmlCode);
        formPdf1.append("reciever",reciever.value);
        formPdf1.append("number",PONO);
         $.ajax({
       url: "PHPMailer-master/sample.php",
       type: "POST",
       data: formPdf1,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
          if(response.includes("Message has been sent")){
            alert("Mail have been sent");
          }

              },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });
      }
      else{
        alert("Please enter mail id");
      }
    }
      
      function mailPdf(){
        
         
       if(PONO!=undefined){
        $('removeContent').remove();
        var mailInput="<div class='row'><div class='col-sm-4'><input type='text'   class='form-control' id='mailId'/></div><div class='col-sm-4'><input type='button'   class='btn btn-primary btn-sm-4' onclick='mailer()' value='Submit'/></div></div></div>";
        $('mailer').append(mailInput);
       
      }
      else{
        alert("Please create one order");
      }
    }
      function myFunction(count){

        
        var cols = "";
        var temp;
        if(count==-2){
          temp=count;
          count=0;
        }
        else{
          temp=count;
          count+=1;
        }

        if(rowCount.has(count)){
            sizeQuantityTemp = rowCount.get(count);
            sizeQuantityTemp++;
            rowCount.set(count,sizeQuantityTemp);
            
        }
        else{
          sizeQuantityTemp=0;
          rowCount.set(count,sizeQuantityTemp);
        }


var newRow = $('<tr id="row'+count+""+sizeQuantityTemp+'">');

var id = 'row'+count+""+sizeQuantityTemp;

cols += '<td class="col-sm-4"><div class="form-group" ><select class="form-control" id="size'+count+""+sizeQuantityTemp+'"></select></div></td>';
cols += '<td><input type="text" name="phone"  class="form-control" id="Quantity'+count+"" + sizeQuantityTemp + '"/></td><td><input type="button" onclick="deleteRow('+id+')" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
         


        
        newRow.append(cols);
        
  
        
          if(temp==-2){
            $("#myTableda").append(newRow);  
          }
          else{
            $("#myTableda"+temp).append(newRow);
          }

          var ListSizeArr = '<?php echo json_encode($list);?>';
          var temp=ListSizeArr.replace(/","/g,",");
          var temp2=temp.replace('["',"");
          var temp3=temp2.replace('"]',"");
          Lists = temp3.split(",");
          for (var i = 0; i < Lists.length; i++) {
            var dynamicId='size'+count+""+sizeQuantityTemp;
            var x = document.getElementById(dynamicId);
            var option = document.createElement("option");
            option.text = Lists[i];
            option.value=i+1;
            x.add(option);
          }
         

      
      }
      function deleteRow(data){
       $(data).remove();
      }
      function addNewBlock(){
        counterNewRow++;
        
        
        counterSizeQuantity=-1;
         var newRow = $('#right_col');
      var cols = '';
      cols +='<form class="userBlock">    <fieldset>  <legend></legend>        <div class="">            <div class="page-title">  <span class="border border-success"><div class="container">    <table id="myTable'+(counterNewRow)+'" class=" table order-list">      <thead>        <tr>            <th width="11%">Style</th>      <th width="18%">FabricColour</th>            <th width="10%">Brand</th>            <th width="11%">Image</th>      <th width="10%">DesignName</th>            <th width="9%">QualityInstructions</th>      <th width="9%">Comments</th>              </tr>    </thead>    <tbody>        <tr>            <td >                <input type="text" name="name" class="form-control" id="style'+counterNewRow+'"/>            </td>            <td >                <div class="form-group">                                                <div class="col-sm-12">                          <div class="input-group demo2">                            <input type="text" value="#e01ab5" class="form-control" id="color'+counterNewRow+'"/>                            <span class="input-group-addon"><i></i></span>                          </div>                        </div>                      </div>            </td>            <td >                <input type="text" name="Brand"  class="form-control" id="Brand'+counterNewRow+'"/>            </td>      <td >        <span class="btn btn-default btn-file">        <input type="file" id="images'+counterNewRow+'">        </span>                    </td>      <td >                <input type="text"   class="form-control" id="DesignName'+counterNewRow+'"/>            </td>            <td >                <input type="text" name="phone"  class="form-control" id="QualityInstructions'+counterNewRow+'"/>            </td>      <td >                <input type="text" name="phone"  class="form-control" id="comments'+counterNewRow+'"/>            </td>            <td class="col-sm-2"><a class="deleteRow"></a>            </td>        </tr>    </tbody>    </table></div><div class="container">    <table id="myTableda'+counterNewRow+'" class=" table order-list">    <thead>        <tr>            <td>Size</td>            <td>Quantity</td>            <td> <p>        <input type="button" onClick="myFunction('+counterNewRow+')" class="addnewrow0 btn btn-md btn-primary "  value="Add">               </button>      </p>            </td>        </tr>    </thead>    <tbody>            </tbody>    <tfoot>                   </tfoot></table></div></span>         <div class="col-sm-5">                </div>                    <div class="col-sm-5">                </div>      </div>    </div>    </fieldset></form>';
      
      newRow.append(cols);
      //temp++;
      $("rags").append(cols);

      }


    function fetchValues(){

      //Fetching PurchaseOrderNumber

      
      var iteratorRow;
      var style;
      /*var vendorName;
      var date;
      var dueDate;*/
      var color; 
      var brand; 
      var imagesda;
      var designName; 
      var qualityInstructions;
      var comment;
      var vendor = document.getElementById('vendor');

      var dueDate = document.getElementById('duedate');
      var date =document.getElementById('date');
      var buyer = document.getElementById('buyer');
      
      var buyerId=buyer.value;
      var buyerName = buyerLists[buyerId-1];
      var vendorId=vendor.value;
      var vendorName = VendorLists[vendorId-1];
      date=date.value;
      dueDate=dueDate.value;
      var checkVendor = new FormData();
      checkVendor.append("vendor",vendorName);
      var vendorPresent;
      $.ajax({
       url: "vendorCheck.php",
       type: "POST",
       data: checkVendor,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
        console.log(response);
          if(response.includes("Present")){
            vendorPresent=true;
          }
          else{
            vendorPresent=false;
            alert("First Create Vendor ")
          }
           
       },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });
      if(vendorPresent){
      var formData = new FormData();
      formData.append("buyer",buyerName);
      formData.append("vendor",vendorName);
      formData.append("Dates",date);
      formData.append("Duedate",dueDate);
      $.ajax({
       url: "poNO.php",
       type: "POST",
       data: formData,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
        
          var temp = JSON.stringify(response);
          var tempr=temp.replace(/ /g,"");
          
          PONO=tempr.replace(/(?:\\[rn]|[\r\n]+)+/g, "");

           
       },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });
        

      
      for(iteratorRow=-1;iteratorRow<=counterNewRow;iteratorRow++){
        
        if(iteratorRow==-1){
          style = document.getElementById('style');
          
          color = document.getElementById('color');
          brand = document.getElementById('Brand');
          imagesda = document.getElementById('images').files[0];
          designName = document.getElementById('DesignName');
          qualityInstructions = document.getElementById('QualityInstructions');
          comment = document.getElementById('comments');


        }   
        else{

          style = document.getElementById('style'+iteratorRow);
          color = document.getElementById('color'+iteratorRow);
          brand = document.getElementById('Brand'+iteratorRow);
          imagesda= document.getElementById('images'+iteratorRow).files[0];
          designName= document.getElementById('DesignName'+iteratorRow);
          qualityInstructions = document.getElementById('QualityInstructions'+iteratorRow);
          comment = document.getElementById('comments'+iteratorRow);

        }
        var i;
        var QuantityArr =[];
        var SizeArray = [];
        var check=0;
        var temporaryIteratorRow=iteratorRow+1;
        var sizeFetch = document.querySelectorAll('[id^=size'+temporaryIteratorRow+']');
        var quantityFetc  = document.querySelectorAll('[id^=Quantity'+temporaryIteratorRow+']');
        var quantityWrite,sizeWrite;
        for(var i in quantityFetc){
           quantityWrite = quantityFetc[i].value;
           /* do your thing */
           
           if(quantityWrite!=undefined){
             QuantityArr.push(quantityWrite);

           }
           
        }
        for(var i in sizeFetch){
           sizeWrite = sizeFetch[i].value;
           /* do your thing */
           var addingElement = Lists[sizeWrite-1];
           if(addingElement!=undefined){
           
            SizeArray.push(addingElement);
          }
           
        }
        
      //Fetchin values of row and column
     
        
       /* for(i=0;i<=rowCount.get(temporaryIteratorRow);i++){
          var QuantityTemp;
          var elementTemp;
          QuantityTemp=document.getElementById("Quantity"+temporaryIteratorRow+""+i);
          elementTemp=document.getElementById("size"+temporaryIteratorRow+""+i);
          console.log()
          if(QuantityTemp!=undefined){
            
            //TO Obtain proper size from element
            var addingElement = Lists[elementTemp.value-1];
            SizeArray.push(addingElement);
          }
         
        }*/
        var jsonSizeArr = JSON.stringify(SizeArray);
        var jsonQuantityArr=JSON.stringify(QuantityArr);
        console.log(jsonSizeArr);
        console.log(jsonQuantityArr);
        //Uploading all values to DB

        var formData1 = new FormData();
        formData1.append("Size",jsonSizeArr);
        formData1.append("Quantity",jsonQuantityArr);
        formData1.append("PONO",PONO);
        formData1.append("file", imagesda);
        formData1.append("fabric", color.value);
        formData1.append("brnd", brand.value);
        formData1.append("design", designName.value);
        formData1.append("quality", qualityInstructions.value);
        formData1.append("Comment", comment.value);
        formData1.append("Style",style.value);
        formData1.append("Dates",date);
        formData1.append("Duedate",dueDate);
        formData1.append("vendor",vendorName);
        
        $.ajax({
         url: "newVendor.php",
         type: "POST",
         data: formData1,
         processData: false,
         contentType: false,
         async: false,
         success: function(response) {
            console.log(response);
             check=1;
         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
        });

     
    }

    if(check==1){
    var formDisplay = new FormData();
      formDisplay.append("POno",PONO);

      $.ajax({
       url: "viewer.php",
       type: "POST",
       data: formDisplay,
       processData: false,
       contentType: false,
       success: function(response) {
        
            popupWindow = window.open("",'popUpWindow','height=3000,width=4000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
         popupWindow.document.write(response);
       },
       error: function(jqXHR, textStatus, errorMessage) {
           console.log(errorMessage); // Optional
       }
    });
    }
    $('create').remove(); 
    var func = '<div class="col-sm-4"><input type="button" name="register" class="btn btn-primary btn-user btn-block" id="print" value="Download" onclick="printFunction()"></div><div class="col-sm-4"><input type="button" name="register" class="btn btn-primary btn-user btn-block" id="mail" value="mail" onclick="mailPdf()"></div>';
    $('functionalities').append(func);
    }
  }

    

  
	</script>
  <script>
    
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
	$('#myDatepicker3').datetimepicker({
        format: 'DD.MM.YYYY'
    });
</script>

  </body>
</html>
     <?php

  
  }
else
{
  unset($_SESSION['user'], $_SESSION['pass'], $_SESSION['last_time']);
  header('Location:loginView.php');
}



  
?>