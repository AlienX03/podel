<?php
	include('dbConfig.php'); // Database connection
		$vendorName = $_POST['vendor'];
		$todayDate = date("yy-m-d");
		$queryExecuter = "select p.POno PONo,p.name Vendor,sum(res.orders) Orders,res.Qty from (select SUBSTRING_INDEX(POno,'_', 1) name,POno from serverdb.finalpo) p,(select count(orders) orders ,PONo , sum(quantity) Qty from (select count(orderNo) orders,POno,sum(quantity) quantity from serverdb.sizequantity group by orderNO) nesres group by nesres.POno) res where p.POno=res.POno and p.name = '".$vendorName."';";
		$results=$dbh->prepare($queryExecuter);
		$results->execute();
        $row = $results->fetch(PDO::FETCH_ASSOC);
        
        $order = $row['Orders'];
		

		$pending = "select p.POno PONo,p.name Vendor,count(res.orders) Orders,sum(res.Qty) Qty from 
		(select SUBSTRING_INDEX(POno, '_', 1) name,POno from serverdb.finalpo) p,
		(select POno,SUBSTRING_INDEX(POno, '_', 1) Vendor,orderNo orders,sum(quantity) Qty 
		from serverdb.sizequantity where quantity>0 group by orderNo) res where p.POno=res.POno and p.name='".$vendorName."' ";
		$pendingResult = $dbh->prepare($pending);
		$pendingResult->execute();
		$pendingRow = $pendingResult->fetch(PDO::FETCH_ASSOC);
		$pendingOrder = $pendingRow['Orders'];

		$delivered = "select count(delivered.Bid) Orders from (select (@rownum:=@rownum+1) Bid,buyer.date Date,bd.vendor,buyer.name Buyer,buyer.orderno Orders,sum(sizequantity.quantity) Qty from 
(select date,name,orderno,id from serverdb.buyerdb) buyer,(SELECT SUBSTRING_INDEX(l.orderno, '_', 1) vendor,orderno from serverdb.buyerdb l) bd,
(select size,quantity,bid from serverdb.sizequantitybuyer) sizequantity,
 (select @rownum:=0) r 
where buyer.id=sizequantity.bid and bd.orderno=buyer.orderno and bd.vendor='".$vendorName."' group by Orders) delivered";
		$deliveredResult = $dbh->prepare($delivered);
		$deliveredResult->execute();
		$deliveredRow = $deliveredResult->fetch(PDO::FETCH_ASSOC);
		$deliveredOrder = $deliveredRow['Orders'];

		$OverDue = "select count(overdue.No) Orders from(select @rownum:=@rownum+1 No,field.dates,field.POno,field.orderNo,field.vendor,sum(size.quantity) quantity from 
(select vd.duedate dates,i.POno POno,i.orderNo orderNo,ven.vendor vendor from serverdb.vendordateduedate vd,serverdb.inputfields i,
(SELECT SUBSTRING_INDEX(l.POno, '_', 1) vendor,l.POno POno from (select distinct POno from serverdb.inputfields) l ) ven   
where vd.POno=i.POno and ven.POno=vd.POno and i.POno=ven.POno and vd.ordeNo=i.orderNo and ven.vendor='".$vendorName."') field,(select orderNo,sum(quantity) quantity 
from serverdb.sizequantity  group by orderNo) size, (select @rownum:=0) r where size.orderNo=field.orderNo and str_to_date(field.dates,'%d.%m.%Y')< '".$todayDate."' group by size.orderNo) overdue";

        $OverDueResult = $dbh->prepare($OverDue);
        $OverDueResult->execute();
		$OverDueRow = $OverDueResult->fetch(PDO::FETCH_ASSOC);
		$OverDueOrder = $OverDueRow['Orders'];

		echo $order.','.$pendingOrder.','.$deliveredOrder.','.$OverDueOrder ;
?>
