<?php
include('dbConfig.php');

try {
	$queryExecuter = "SELECT * from amazon.category";
    $results=$dbh->prepare($queryExecuter);
}
catch(Exception $e){
	print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PODEL</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
   <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PODEL </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
   <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
            </div>

            <div class="clearfix"></div>



            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <div id="name" class="text-uppercase">
				</div>
              </div>
            </div>
            <!-- /menu profile quick info -->

            

         <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                      <li><a href="index.html">Dashboard</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a href="tables.php">Purchase Order</a></li>
                      <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                    <ul class="nav child_menu">
                      <li><a href="settings.php">Business Info</a></li>
                      <li><a href="vendor.php">Vendors</a></li>
                      <li><a href="buyer.php">Buyers</a></li>
                      <li><a href="style.php">Style</a></li>
                      <li><a href="category.php">Category</a></li>
                    </ul>
                  </li>
                      <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
					   <ul class="nav child_menu">
                      <li><a href="POsVendor.php">All POs</a></li>
                      <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                      <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>
                      
					  </li>
                    </ul>
                </ul>
              </div>
            </div>   <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
				
                  <a href="javascript:;" class="user-profile dropdown-toggle text-uppercase" data-toggle="dropdown" aria-expanded="false" id="names">
                    
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
		<!-- PageContentStart -->
		<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
					<form class="user">
						<div class="row form-group top_search">
						<div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action" id="ProfileInfo">
                        <tbody class="gfg">
						
						  <tr>
							<td style="padding-left: 5px;padding-bottom: 10px;">
								<strong style="font-size: 20px;">Style Name
								<strong style="font-size: 20px; color: red;">*</strong>
								</strong><br />
							</td>
							
								<td style="font-size: 20px; color: #262626">
									<input class="form-control" type="text" id="StyleName">
								</td>
					
							                            
						  </tr>
						  <tr>
							<td style="padding-left: 5px;padding-bottom: 10px;">
								<strong style="font-size: 20px;">Category
								<strong style="font-size: 20px; color: red;">*</strong>
								<button type="button" class="btn btn-round btn-sm btn-info glyphicon glyphicon-plus pull-right" data-toggle="modal" data-target="#myModal"></button></strong><br />
								 
							</td>
						  <td style="font-size: 20px; color: #262626">
								<div >
                          <select class="form-control" id="Category">
                            <option>Select Category</option>
							<?php if ($results->execute()) {
							while ($row = $results->fetch(PDO::FETCH_ASSOC))  { ?>
                            <option value="<?php echo $row ['id']; ?>"><?php echo $row ['category']; ?></option>
                            <?php }} ?>
                          </select>
                        </div>
                      </div>
							</td>
						  </tr>
						  <tr>
							<td style="padding-left: 5px;padding-bottom: 10px;">
								<strong style="font-size: 20px;">Gender
<strong style="font-size: 20px; color: red;">*</strong>								</strong><br />
							</td>
						  <td style="font-size: 20px; color: #262626">
								<div >
                          <select class="form-control" id="Gender">
                            <option>Select your Gender</option>
							
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                          </select>
                        </div>
							</td>
						  </tr>
						  
                        </tbody>
						
                      </table>
                    </div>
						</div>
					</form>
				</div>
			</div>
		 </div>
		</div>
		<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">New Style</h4>
		  
          <button type="button" class="close" data-dismiss="modal"></button>
		  
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
         <div class="input-group">
								<input type="text" class="form-control" placeholder="StyleName" id="newStyle">
								<span class="input-group-btn">
								<button class="btn btn-default" type="button" id="newStyleUpdate">Update!</button>
								</span>
								</div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
		
		
		
		<!-- PageContentEnd -->
		<!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
	<script src="js/bootstable.js"></script>
<script type="text/javascript" src="js/editable.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
  </body>
  
  <br><br><br>

</html>
