<?php
include('dbConfig.php');
session_start();
try {
	$queryExecuter = "SELECT id,name FROM serverdb.vendorinfo;";
    $results=$dbh->prepare($queryExecuter);
}
catch(Exception $e){
	print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PODEL</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
   <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>
  <html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PODEL </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
   <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="indexView.php" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
            </div>

            <div class="clearfix"></div>



            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <div id="name" class="text-uppercase">
				</div>
              </div>
            </div>
            <!-- /menu profile quick info -->

            

         <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                      <li><a href="indexView.php">Dashboard</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a href="tables.php">Purchase Order</a></li>
                      <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                    <ul class="nav child_menu">
                      <li><a href="settings.php">Business Info</a></li>
                      <li><a href="vendor.php">Vendors</a></li>
                      <li><a href="buyer.php">Buyers</a></li>
                      <li><a href="size.php">Size</a></li>
                    </ul>
                  </li>
                      <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
					   <ul class="nav child_menu">
                      <li><a href="POsVendor.php">All POs</a></li>
                      <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                      <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>
                      
					  </li>
                    </ul>
                </ul>
              </div>
            </div>  <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-large ">
                          
                          <a data-toggle="tooltip" data-placement="top" title="Logout" href="loginView.php">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                          </a>
                        </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
       <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              
            </nav>
          </div>
        </div>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
					<form class="user">
					
						<div class="row form-group top_search">
							
							<div class="col-sm-4">
								<div class="input-group">
								<input type="text" class="form-control" placeholder="Vendor" id="vendor">
								<span class="input-group-btn">
								<button class="btn btn-default" type="button" id="VendorButton">Go!</button>
								</span>
								</div>
							</div>
							
							<div class="col-sm-4">
								<a href="onClickNewVendor.html"><button  class="btn btn-primary" type="button" id="CreateNew">CreateNew</button></a>
							</div>
							
							</div>	
				
				
				 			
						
					</form>
              </div>

              
            </div>

            

            

              <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Vendor</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action" id="editableTable">
                        <thead>
                          <tr class="headings">
                            
                            <th class="column-title">Sno </th>
                            <th class="column-title">Vendor </th>
                            <th class="column-title no-link last"><span class="nobr">Actions</span>
                            </th>
                            
                          </tr>
                        </thead>
						<?php if ($results->execute()) {
							while ($row = $results->fetch(PDO::FETCH_ASSOC))  { ?>
							<tr id="<?php echo $row ['id']; ?>">

								<td><?php echo $row ['id']; ?></td>
								<td><?php echo $row ['name']; ?></td>  	
								<td><button type="button" class="btn btn-primary" onclick="view(<?php echo $row['id']?>)"><span class="glyphicon glyphicon-eye-open"></span></button>
						<?php }} ?>
                      </table>
                    </div>
							
						
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            PODEL - Aura
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
	<script src="js/bootstable.js"></script>
<script type="text/javascript" src="js/newVendor.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
    <script >
      window.view = function(data){
        var formData = new FormData();
        formData.append("id",data);
        $.ajax({
         url: "vendorView.php",
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         success: function(response) {
         popupWindow = window.open("",'popUpWindow','height=3000,width=4000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
         popupWindow.document.write(response);

         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });
      }

    </script>
  </body>
  
  <br><br><br>

</html>
