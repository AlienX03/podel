<?php

include('dbConfig.php'); // Database connection
session_start();
$user = $_SESSION["name"];  
if(isset($_SESSION["name"]))
{ 
      //Retrieving vendor list for list options
      $vendorName = "SELECT id,name FROM serverdb.vendorinfo;";
      $vendorResults=$dbh->prepare($vendorName);
      $vendorNames = [];
      if ($vendorResults->execute()) {
        while ($vendorRow = $vendorResults->fetch(PDO::FETCH_ASSOC)) {
            array_push($vendorNames, $vendorRow['name']);
        }
      }
      $temp=0;
  
  
    //echo"<script>alert(".time().");</script>";
        $_SESSION['last_time'] = time(); //set new timestamp
        $_SESSION['logged_in'] = false;
        $userid = $_SESSION['uid'];
        $todayDate = date("yy-m-d");
        try {

          if($_SESSION['type']==1){    
            $queryExecuter = "SELECT count(orders) Orders FROM serverdb.finalorderno;" ;
            $results=$dbh->prepare($queryExecuter);
            $pending = "select count(rem.orderNo) Orders from (SELECT orderNo,sum(quantity) qty,sum(pending) pen FROM serverdb.sizequantity group by orderNO) rem where rem.pen = 0";
            $pendingResult = $dbh->prepare($pending);
            $delivered = "select count(delivered.Bid) Orders from (select (@rownum:=@rownum+1) Bid,buyer.date Date,buyer.name Buyer,buyer.orderno Orders,sum(sizequantity.quantity) Qty from (select date,name,orderno,id from serverdb.buyerdb) buyer,(select size,quantity,bid from serverdb.sizequantitybuyer) sizequantity, (select @rownum:=0) r where buyer.id=sizequantity.bid group by Orders) delivered";
            $deliveredResult = $dbh->prepare($delivered);
            
            $OverDue = "select count(overdue.No) Orders from(select @rownum:=@rownum+1 No,field.dates,field.POno,field.orderNo,field.vendor,sum(size.quantity) quantity from (select vd.duedate dates,i.POno POno,i.orderNo orderNo,ven.vendor vendor from serverdb.vendordateduedate vd,serverdb.inputfields i,(SELECT SUBSTRING_INDEX(l.POno, '_', 1) vendor,l.POno POno from (select distinct POno from serverdb.inputfields) l) ven   where vd.POno=i.POno and ven.POno=vd.POno and i.POno=ven.POno and vd.ordeNo=i.orderNo) field,(select orderNo,sum(quantity) quantity from serverdb.sizequantity  group by orderNo) size, (select @rownum:=0) r where size.orderNo=field.orderNo and str_to_date(field.dates,'%d.%m.%Y')< '".$todayDate."' group by size.orderNo) overdue";
            $OverDueResult = $dbh->prepare($OverDue);
          }
          else{
            //vendor name
            $vendorId = $_SESSION['vendorId'];

            //Fetching vendor name from vendor id
            $vendorIdQuery="SELECT * FROM serverdb.vendorinfo where id = '".$vendorId."'";
            $vendorIdPrepare = $dbh->prepare($vendorIdQuery);
            $vendorIdPrepare->execute();
            $vendorIdResult = $vendorIdPrepare->fetch(PDO::FETCH_ASSOC);
            $vendorName = $vendorIdResult['name'];

            $queryExecuter = "select p.POno PONo,p.name Vendor,sum(res.orders) Orders,res.Qty from (select SUBSTRING_INDEX(POno,'_', 1) name,POno from serverdb.finalpo) p,(select count(orders) orders ,PONo , sum(quantity) Qty from (select count(orderNo) orders,POno,sum(quantity) quantity from serverdb.sizequantity group by orderNO) nesres group by nesres.POno) res where p.POno=res.POno and p.name = '".$vendorName."';";
            $results=$dbh->prepare($queryExecuter);

            $pending = "select sum(pending.Orders) Orders from(select p.POno PONo,p.name Vendor,count(res.orders) Orders,sum(res.Qty) Qty from (select SUBSTRING_INDEX(POno, '_', 1) name,POno from serverdb.finalpo) p, (select POno,SUBSTRING_INDEX(POno, '_', 1) Vendor,orderNo orders,sum(quantity) Qty from serverdb.sizequantity where pending=0 group by orderNo) res where p.POno=res.POno and p.name='".$vendorName."' group by p.POno) pending;";

            $pendingResult = $dbh->prepare($pending);

           $delivered = "select count(delivered.Bid) Orders from (select (@rownum:=@rownum+1) Bid,buyer.date Date,bd.vendor,buyer.name Buyer,buyer.orderno Orders,sum(sizequantity.quantity) Qty from 
(select date,name,orderno,id from serverdb.buyerdb) buyer,(SELECT SUBSTRING_INDEX(l.orderno, '_', 1) vendor,orderno from serverdb.buyerdb l) bd,
(select size,quantity,bid from serverdb.sizequantitybuyer) sizequantity,
 (select @rownum:=0) r 
where buyer.id=sizequantity.bid and bd.orderno=buyer.orderno and bd.vendor='".$vendorName."' group by Orders) delivered";
            $deliveredResult = $dbh->prepare($delivered);
            
            $OverDue = "select count(overdue.No) Orders from(select @rownum:=@rownum+1 No,field.dates,field.POno,field.orderNo,field.vendor,sum(size.quantity) quantity from 
(select vd.duedate dates,i.POno POno,i.orderNo orderNo,ven.vendor vendor from serverdb.vendordateduedate vd,serverdb.inputfields i,
(SELECT SUBSTRING_INDEX(l.POno, '_', 1) vendor,l.POno POno from (select distinct POno from serverdb.inputfields) l ) ven   
where vd.POno=i.POno and ven.POno=vd.POno and i.POno=ven.POno and vd.ordeNo=i.orderNo and ven.vendor='".$vendorName."') field,(select orderNo,sum(quantity) quantity 
from serverdb.sizequantity  group by orderNo) size, (select @rownum:=0) r where size.orderNo=field.orderNo and str_to_date(field.dates,'%d.%m.%Y')< '".$todayDate."' group by size.orderNo) overdue";

            $OverDueResult = $dbh->prepare($OverDue);


          }
          
        }
            catch(Exception $e){
                print "Error!: " . $e->getMessage() . "<br/>";
                die();
            }
            ?>
            <!DOCTYPE html>
            <html lang="en">
              <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Meta, title, CSS, favicons, etc. -->
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
            	<link rel="icon" href="images/favicon.ico" type="image/ico" />

                <title>PODEL</title>

                <!-- Bootstrap -->
                <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
                <!-- Font Awesome -->
                <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
                <!-- NProgress -->
                <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
                <!-- iCheck -->
                <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">

                <!-- bootstrap-progressbar -->
                <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
                <!-- JQVMap -->
                <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
                <!-- bootstrap-daterangepicker -->
                <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

                <!-- Custom Theme Style -->
                <link href="../build/css/custom.min.css" rel="stylesheet">
              </head>

              <body class="nav-md">

                <div class="container body">
                  <div class="main_container">
                    <div class="col-md-3 left_col">
                      <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                          <a href="indexView.php" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">

                          <div class="profile_info">
                            <span>Welcome,</span>
                            <div id="name" class="text-uppercase">
            				</div>
                          </div>
                        </div>
                        <!-- /menu profile quick info -->



                        <!-- sidebar menu -->
                     <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                          <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                  <li><a href="indexView.php">Dashboard</a></li>
                                </ul>
                                <ul class="nav side-menu">
                                  <li><a href="tables.php">Purchase Order</a></li>
                                  <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                                </ul>
                                <?php if($_SESSION['type']==1) { ?>
                                <ul class="nav side-menu">
                                  <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                                <ul class="nav child_menu">
                                  <li><a href="settings.php">Business Info</a></li>
                                  <li><a href="vendor.php">Vendors</a></li>
                                  <li><a href="buyer.php">Buyers</a></li>
                                  <li><a href="size.php">Size</a></li>
                                </ul>
                              </li>
                                  <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
            					   <ul class="nav child_menu">
                                  <li><a href="POsVendor.php">All POs</a></li>
                                  <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                                  <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>

            					  </li>
                                </ul>
                              <?php } ?>
                            </ul>
                          </div>
                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-large ">
                          
                          <a data-toggle="tooltip" data-placement="top" title="Logout" href="loginView.php">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                          </a>
                        </div>
                        <!-- /menu footer buttons -->
                      </div>
                    </div>

                    <!-- top navigation -->
                    
                    <!-- /top navigation -->

                    <!-- page content -->
                    <div class="right_col" role="main">
                      <!-- top tiles -->
                      <?php if($_SESSION['type']==1){ ?>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="input-group">
                                <div class="form-group" ><select class="form-control" id="vendor"></select></div>
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="onFilter()">Go!</button>
                                  </span>
                              </div>
                            </div>
                          </div>
                      <?php } ?>
                      <div class="row tile_count">
                        
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                          <span class="count_top"><i class="fa fa-user"></i> Total Orders</span>
                          <?php if ($results->execute()) {
                            $row = $results->fetch(PDO::FETCH_ASSOC)
                           ?>
                          <div class="count"><order><?php echo $row ['Orders']; ?></order></div>
                        <?php } ?>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                          <span class="count_top"><i class="fa fa-clock-o"></i> Pending</span>
                         <?php if ($pendingResult->execute()) {
                            $pendingRow = $pendingResult->fetch(PDO::FETCH_ASSOC)
                           ?>
                          <div class="count"><pendingorder><?php echo $pendingRow ['Orders']; ?></pendingorder></div>
                        <?php } ?>
                        </div>
                         <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                          <span class="count_top"><i class="glyphicon glyphicon-ok"></i> Delivered</span>
                         <?php if ($deliveredResult->execute()) {
                            $deliveredRow = $deliveredResult->fetch(PDO::FETCH_ASSOC);
                            
                           ?>
                          <div class="count"><delivery><?php 
                          if(!empty($deliveredRow)){
                            echo $deliveredRow ['Orders'];  
                          }
                          else {
                            echo '0';
                          }?></delivery></div>
                        <?php } ?>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                          <span class="count_top"><i class="glyphicon glyphicon-calendar"></i> Over Due</span>
                         <?php if ($OverDueResult->execute()) {
                            $overDueRow = $OverDueResult->fetch(PDO::FETCH_ASSOC)
                           ?>
                          <div class="count"><overdue><?php echo $overDueRow ['Orders']; ?></overdue></div>
                        <?php } ?>
                        </div>
                      </div>
                      <!-- /top tiles -->





                          </div>
                        </div>
                      </div>

                    </div>
                    <!-- /page content -->

                    
                  </div>
                </div>

                <!-- jQuery -->
                <script src="../vendors/jquery/dist/jquery.min.js"></script>
                <!-- Bootstrap -->
                <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
                <!-- FastClick -->
                <script src="../vendors/fastclick/lib/fastclick.js"></script>
                <!-- NProgress -->
                <script src="../vendors/nprogress/nprogress.js"></script>
                <!-- Chart.js -->
                <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
                <!-- gauge.js -->
                <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
                <!-- bootstrap-progressbar -->
                <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
                <!-- iCheck -->
                <script src="../vendors/iCheck/icheck.min.js"></script>
                <!-- Skycons -->
                <script src="../vendors/skycons/skycons.js"></script>
                <!-- Flot -->
                <script src="../vendors/Flot/jquery.flot.js"></script>
                <script src="../vendors/Flot/jquery.flot.pie.js"></script>
                <script src="../vendors/Flot/jquery.flot.time.js"></script>
                <script src="../vendors/Flot/jquery.flot.stack.js"></script>
                <script src="../vendors/Flot/jquery.flot.resize.js"></script>
                <!-- Flot plugins -->
                <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
                <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
                <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
                <!-- DateJS -->
                <script src="../vendors/DateJS/build/date.js"></script>
                <!-- JQVMap -->
                <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
                <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
                <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
                <!-- bootstrap-daterangepicker -->
                <script src="../vendors/moment/min/moment.min.js"></script>
                <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
            <script type="text/javascript" src="js/index.js"></script>
                <!-- Custom Theme Scripts -->
                <script src="../build/js/custom.min.js"></script>
                <script>

                  var ListSizeArr = '<?php echo json_encode($vendorNames);?>';
                  console.log(ListSizeArr);
                  var temp=ListSizeArr.replace(/","/g,",");
                  var temp2=temp.replace('["',"");
                  var temp3=temp2.replace('"]',"");
                  Lists = temp3.split(",");
                  var dynamicId="vendor";
                  var x = document.getElementById(dynamicId);
                  var options = document.createElement("option");
                  options.text = "Select vendor name";
                  options.value=0;
                  x.add(options);
                  for (var i = 0; i < Lists.length; i++) {
                    
                    var option = document.createElement("option");
                    option.text = Lists[i];
                    option.value=i+1;
                    x.add(option);
                  }
                  window.onFilter = function(){
                    var vendorIds = document.getElementById('vendor').value;
                    var vendorName = Lists[vendorIds-1];
                    if(vendorName!=undefined){
                        $('order').empty();
                        $('pendingorder').empty();
                        $('delivery').empty();
                        $('overdue').empty();
                        var orderValue;
                        var pendingOrderValue;
                        var deliveredValue;
                        var overDueValue;
                        var formData = new FormData();
                        formData.append("vendor",vendorName);
                        $.ajax({
                          url: "indexValue.php",
                          type: "POST",
                          data: formData,
                          processData: false,
                          contentType: false,
                          async: false,
                          success: function(response) {
                              var temp = response.split(",");
                              
                              orderValue = temp[0];
                              pendingOrderValue = temp[1];
                              deliveredValue = temp[2];
                              overDueValue = temp[3];
                          },
                          error: function(jqXHR, textStatus, errorMessage) {
                              console.log(errorMessage); // Optional
                          }
                        });
                        $('order').append(orderValue);
                        $('pendingorder').append(pendingOrderValue);
                        $('delivery').append(deliveredValue);
                        $('overdue').append(overDueValue);
                    }
                   
                  }
                </script>
              </body>
            </html>
            <?php

  
  }
else
{
  unset($_SESSION['user'], $_SESSION['pass'], $_SESSION['last_time']);
  header('Location:loginView.php');
}



  
?>