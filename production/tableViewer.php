<?php 
	session_start();
	include('dbconfig.php');
	$POno = $_POST['POno'];
	//$POno = 'Ragul_2_JAN2020';
	//Fetching id from po number
	$vendorIdQuery = "SELECT vid FROM serverdb.finalpo where POno = ".$POno." limit 1";
    $vendorIdPrepare = $dbh->prepare($vendorIdQuery);
    $vendorIdPrepare->execute();
    $vendorIdresult=$vendorIdPrepare->fetch(PDO::FETCH_ASSOC);
    $vendorId = $vendorIdresult['vid'];

    //fetching buyer and vendor name from vendor id
    $purchaseOrderQuery = "SELECT vendor,buyer FROM serverdb.vendordb where id='".$vendorId."';";
    $results=$dbh->prepare($purchaseOrderQuery);
    $results->execute();
    $row = $results->fetch(PDO::FETCH_ASSOC);

    //fetching dates from purchase order number
    $datesQuery = "SELECT date,duedate FROM serverdb.vendordateduedate where POno =".$POno." limit 1";
    $datesResults=$dbh->prepare($datesQuery);
    $datesResults->execute();
    $dateRow = $datesResults->fetch(PDO::FETCH_ASSOC);
    

    $orderNumber = [];
    //Fetching corresponding order numbers from table
    $orderQuery = "SELECT * FROM serverdb.finalorderno where POno = ".$POno."";
    $ordersResults = $dbh->prepare($orderQuery);

    if($ordersResults->execute()){
    	while($orderRow = $ordersResults->fetch(PDO::FETCH_ASSOC)){
    		array_push($orderNumber,$orderRow['orders']);
    	}
    }

?>
<!DOCTYPE html>
	<html lang="en">
	  <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	  <head>
	  	
	  </head>
	  <body>
	  		<div class="row">
	  			
	  				<div class="row">
	  					<div class="col-xs-4"><strong style="font-size: 22px;">Vendor Name:</strong></div>
	  					<div class="col-xs-4 pull-left"><p style="font-size: 20px;"><?php echo $row['vendor'];?></p></div>
	  				</div>
	  				<div class="row">
	  					<div class="col-xs-4"><strong style="font-size: 22px;">Buyer Name:</strong></div>
	  					<div class="col-xs-4 pull-left"><p style="font-size: 20px;"><?php echo $row['buyer'];?></p></div>
	  				</div>
	  				<div class="row">
	  					<div class="col-xs-4"><strong style="font-size: 22px;">Date:</strong></div>
	  					<div class="col-xs-4 pull-left"><p style="font-size: 20px;"><?php echo $dateRow['date'];?></p></div>
	  				</div>
	  				<div class="row">
	  					<div class="col-xs-4"><strong style="font-size: 22px;">DueDate :</strong></div>
	  					<div class="col-xs-4 pull-left"><p style="font-size: 20px;"><?php echo $dateRow['duedate'];?></p></div>
	  				</div>
	  				<div class="row">
	  					<div class="col-xs-4"><strong style="font-size: 22px;">PurchaseOrder No:</strong></div>
	  					<div class="col-xs-4 pull-left"><p style="font-size: 20px;"><?php echo $POno;?></p></div>
	  				</div>
	  			

	  		</div>
            	<?php foreach ($orderNumber as $key => $value) { 


            		$nextedExecuter = "SELECT @rownum:=@rownum+1 No,style,colour,brand,image,design,quality,comment FROM serverdb.inputfields,(select @rownum:=0) r where orderNo='".$value."'";
				    $nestedResults=$dbh->prepare($nextedExecuter);
				    $nestedResults->execute();
				    $orderNestedRow = $nestedResults->fetch(PDO::FETCH_ASSOC);
				    $sizeQuantityQuery = "select size,quantity from serverdb.sizequantity where orderNO='".$value."'";
				    $sizeQuantityResult = $dbh->prepare($sizeQuantityQuery);
				    ?>
            		<hr>
<div class="row" style="padding-left: 50px;">
	  		

            <table class="table table-striped jambo_table bulk_action" id="editableTable">
                <thead>
                    <tr class="headings">
                        <th class="column-title" width="5%">Sno</th>
                        <th class="column-title" width="14%">Order number</th>
                        <th class="column-title" width="11%">Style </th>
                        <th class="column-title" width="11%">Colour </th>
                        <th class="column-title" width="11%">Brand </th>
                        <th class="column-title" width="12%">Image </th>
                        <th class="column-title" width="12%">Design </th>
                        <th class="column-title" width="12%">quality </th>
                        <th class="column-title" width="12%">comment </th>      
                    </tr>
                </thead>
                    <tr id="<?php echo $row ['No']; ?>">
                        <td><?php echo $key+1 ?></td>
                        <td><?php echo $value; ?></td>
                        <td><?php echo $orderNestedRow ['style']; ?></td>
                        <td><?php echo $orderNestedRow ['colour']; ?></td>
                        <td><?php echo $orderNestedRow ['brand']; ?></td>
                        <td><img src="<?php echo $orderNestedRow ['image'];?>" alt='Red Dot' height='64' width='64' border='1'/></td>     
                        <td><?php echo $orderNestedRow['design']; ?></td>
                        <td><?php echo $orderNestedRow ['quality']; ?></td>    
                        <td><?php echo $orderNestedRow ['comment']; ?></td> 
                        
            </table>

	  		
            	<div class="row">
            		<div class="col-xs-2">
            			<STRONG>Size</STRONG>
            		</div>
            		<div class="col-xs-2">
            			<STRONG>Quantity</STRONG>
            		</div>
            	</div>
            	<?php if($sizeQuantityResult->execute()){
            		while($sqRow = $sizeQuantityResult->fetch(PDO::FETCH_ASSOC))  { ?>
            			<div class = "row">
            				<div class="col-xs-2">
            					<p><?php echo $sqRow['size'];?></p>
            					<size></size>
            				</div>
            				<div class="col-xs-2">
            					<p><?php echo $sqRow['quantity'];?></p>
            					<quantity></quantity>
            				</div>

            			</div>

            	
            		
            	<?php }} ?>
	                        
	                       
            </div>
        </div>



            	<?php } ?>


            </div>
	  </body>
	  </html>
