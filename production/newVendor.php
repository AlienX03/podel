<?php
	include('dbConfig.php');
  session_start();
  $style = $_POST['Style'];
  $brand = $_POST['brnd'];
  $size = json_decode($_POST['Size']);
  $POno = $_POST['PONO'];
  $color = $_POST['fabric'];
  $designName = $_POST['design'];
  $quantity = json_decode($_POST['Quantity']);
  $qalityInstructions = $_POST['quality'];
  $comment = $_POST['Comment'];
  $name = $_FILES["file"]["name"];
  $target_dir = "images/";
  $target_file = $target_dir . basename($_FILES["file"]["name"]);
  $dueDate = $_POST['Duedate'];
  $Vendor = $_POST['vendor'];
  $date=$_POST['Dates'];
  $count=count($quantity );
  $userId  = $_SESSION['uid'];
  //Retrieving today's month and year
  $month = date("M");
  $year = date("Y");
  
  

 
  
  

  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  
  $extensions_arr = array("jpg","jpeg","png","gif");

  //updating orders table
    $ordersQuery = "insert into serverdb.orders (months,year,POno,vendor) values ('".$month."','".$year."',".$POno.",'".$Vendor."')";
    $ordersPrepare = $dbh->prepare($ordersQuery);
    $ordersRes = $ordersPrepare->execute();
    
 
    //uploading to final orders table
    $finalOrderQuery = "insert into serverdb.finalorderno(orders,POno) select concat(vendor,'_',id,'_', months,year),POno from serverdb.orders where POno=".$POno." order by id desc limit 1";
    $finalOrderPrepare = $dbh->prepare($finalOrderQuery);
    $finalOrderRes = $finalOrderPrepare->execute();

    //Fetching order number from order table
    $ordernoQuery = "select orders from serverdb.finalorderno where POno=".$POno." order by id desc limit 1";
    $ordernoPrepare = $dbh->prepare($ordernoQuery);
    $ordernoPrepare->execute();
    $ordernoresult=$ordernoPrepare->fetchAll(PDO::FETCH_ASSOC);
    $orderno = $ordernoresult[0]['orders'];

 //Uploading values to date table
  $dateQuery="INSERT INTO `serverdb`.`vendordateduedate` (`POno`, `date`, `duedate`,`ordeNo`) VALUES (".$POno.",'".$date."','".$dueDate."','".$orderno."')";
  $datePrepare = $dbh->prepare($dateQuery);
  $datePrepare->execute();
  echo $dateQuery;

  if( in_array($imageFileType,$extensions_arr) ){
 
    //Uploading to inputfields table
    $image_base64 = base64_encode(file_get_contents($_FILES['file']['tmp_name']) );
    $image = 'data:image/'.$imageFileType.';base64,'.$image_base64;
    $inp = "INSERT INTO `serverdb`.`inputfields` (`PONo`, `style`, `colour`, `brand`, `image`, `design`, `quality`, `comment`, `orderNO`,`sizeQuantity`,`userId`) VALUES (".$POno.", '".$style."', '".$color."', '".$brand."', '".$image."', '".$designName."', '".$qalityInstructions."', '".$comment."', '".$orderno."', '".$count."','".$userId."')";
    $results=$dbh->prepare($inp);
    $res=$results->execute();
    
     // Upload file
    move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
  }

  

  //updating size and quantity to size and quantity table
  foreach ($size as $key => $value) {
    # code...
    

    //updating size and quantity to table
    $sizeQuery = "INSERT INTO `serverdb`.`sizequantity` (`orderNO`, `size`, `quantity`, `POno`, `userID`,`pending`) VALUES ('".$orderno."', '".$value."', '".$quantity[$key]."', ".$POno.",'".$userId."','0')";
    $sizePrepare = $dbh->prepare($sizeQuery);
    $sizePrepare->execute();

    
  }

?>
