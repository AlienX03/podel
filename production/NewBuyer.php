<?php
session_start();
    
?>	
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>PODEL</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="../vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="indexView.php" class="site_title"><i class="fa fa-paw"></i> <span>PODEL</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Welcome,</span>
                <div id="name" class="text-uppercase">
				</div>
              </div>
            </div>
            <!-- /menu profile quick info -->

            

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                      <li><a href="indexView.php">Dashboard</a></li>
                    </ul>
                    <ul class="nav side-menu">
                      <li><a href="tables.php">Purchase Order</a></li>
                      <li><a href="tables_dynamic.php">Dispatch/Delivery</a></li>
                    </ul>
                    <?php if($_SESSION['type']==1) { ?>
                                <ul class="nav side-menu">
                                  <li><a><i class="glyphicon glyphicon-cog"></i> Settings </a>
                                <ul class="nav child_menu">
                                  <li><a href="settings.php">Business Info</a></li>
                                  <li><a href="vendor.php">Vendors</a></li>
                                  <li><a href="buyer.php">Buyers</a></li>
                                  <li><a href="size.php">Size</a></li>
                                </ul>
                              </li>
                                  <li><a><i class="glyphicon glyphicon-file"></i> Reports </a>
                         <ul class="nav child_menu">
                                  <li><a href="POsVendor.php">All POs</a></li>
                                  <li><a href="PendingOrdersOnClick.php">Pending Deliveries</a></li>
                                  <li><a href="PendingDeliveries.php">Pending Deliveries - Detailed</a></li>

                        </li>
                                </ul>
                              <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-large ">
                          
                          <a data-toggle="tooltip" data-placement="top" title="Logout" href="loginView.php">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                          </a>
                        </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
         

             
        <!-- /page content -->
	<div class="right_col" role="main">
        <div class="">
            <div class="page-title">
					<form class="user">
					<div class="form-group row">
					  
				    
					<div class="col-lg-12  form-group top_search">
							
					
                <mailer>
							
							
						<functionalities>
              
          </functionalities>
            
           </mailer>

          
					</div>
								                													
					</form>

<!-- Page Content -->
<div class="container" id="container">

  <rags>
    
  </rags>
</div>


<!-- /.container -->
<Creator>
<div class = "col-xs-12">
              <div class="col-sm-3"><div class="input-group"><input type="text" class="form-control" placeholder="Order Number" id="OrderNO"></div></div>
              <div class = "col-sm-3">
                <input type="button" name="create" class="btn btn-primary btn-user btn-block" onclick="onList()" value="list">
              </div>
  <div class="col-sm-2 col-xs-2 col-l-1 pull-right"><input type="button" name="create" class="btn btn- btn-primary btn-user btn-block pull-right" onclick="onCreate()" value="create"></div>
             
              </div>
            </Creator>
			</div>
		</div>
	</div>
       </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
	<script src="../vendors/moment/min/moment.min.js"></script>
	<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    
<script type="text/javascript" src="js/newBuy.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>
	<script>
    
    
    $('#myDatepicker2').datetimepicker({
        format: 'DD.MM.YYYY'
    });
	$('#myDatepicker3').datetimepicker({
        format: 'DD.MM.YYYY'
    });
</script>
<script >
  var orders =[];
  var buyer;
  var dateArr =[];
  var counterForm = 0;
  var clicksList=0;
var typeOfUser = "<?php echo $_SESSION['type']; ?>";

  window.finalise = function(order,date){
    console.log("entered");
      orders.push(order);
      var start = "start";
      
      var formData = new FormData();
      formData.append("date",date);
      formData.append("order",order);
      formData.append("buyer",buyer);
      formData.append("start",start);
      formData.append("count",counterForm);
      $.ajax({
         url: "new.php",
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         success: function(response) {
          
         counterForm++;
              $("rags").append(response);
         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });
  }
  window.onList = function(){
    
    var ref;
      var buyerName="";
      var date="";
      var order = "";
      var orderNO= document.getElementById("OrderNO");
      order=orderNO.value;
      $('#OrderNO').val("");
      var orderData = new FormData();
      orderData.append("order",order);
      var answer = [];
      $.ajax({
         url: "buyerdate.php",
         type: "POST",
         data: orderData,
         processData: false,
         contentType: false,
         async: false,
         success: function(response) {
         answer = response.split(",");
         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });
     buyerName=answer[0];

      
     if(clicksList==0){
      
      buyer=buyerName.toUpperCase();
      date=answer[2];

        dateArr.push(date);
        finalise(order,date);
     }
     else{
      if(buyerName.toUpperCase()==buyer.toUpperCase()){
        date=answer[2];
        buyer = buyerName.toUpperCase();
        
        dateArr.push(date);
        finalise(order,date);
      }
      else{
        alert("Sorry, You have entered order number of two different buyers");
      }
     }

     

      
      clicksList++;
    }
    function printFunction(){
       
       var htmlCode;
       if(orders!=undefined){
        var tempOrder = JSON.stringify(orders);
        var formData = new FormData();
        formData.append("order",tempOrder);
        formData.append("reference",ref);
        $.ajax({
         url: "dispatchTablePreview.php",
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         async: false,
         success: function(response) {
         
        htmlCode=response;

         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });
      

          
      var formPdf1 = new FormData();
        formPdf1.append("html",htmlCode);
         $.ajax({
       url: "tester.php",
       type: "POST",
       data: formPdf1,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
         popupWindow = window.open("",'popUpWindow','height=3000,width=4000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
         popupWindow.location.href=response;
              },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });    
        }
      }
  window.onCreate = function() {
    
    var check=0;
    $.ajax({
            url: "randomGenerator.php",
            async: false,
            success: function(response) {
                ref=response;
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
            }
        });
    for(var i=0;i<counterForm;i++){
      var neededQuantity = document.getElementsByClassName("quantity"+i);
      var QuantityArr=[];
      for(var j=0;j<neededQuantity.length;j++){
        var temp = document.getElementById(i+""+j);
        if(temp.value==""){
          QuantityArr.push(0);
        }
        else{
          QuantityArr.push(temp.value); 
        }
        
      }
      var jsonQuantity=JSON.stringify(QuantityArr);
      
          var formData = new FormData();
          formData.append("date",dateArr[i]);
          formData.append("order",orders[i]);
          formData.append("buyer",buyer);
          formData.append("Quantity",jsonQuantity);
          formData.append("reference",ref);
          $.ajax({
            url: "buyerProduction.php",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            async:false,
            success: function(response) {
                check=1;
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
            }
        });
           
       
        
     
    }
    if( check==1){
    var tempOrder = JSON.stringify(orders);
    var formData = new FormData();
        formData.append("order",tempOrder);
        formData.append("reference",ref);
        $.ajax({
         url: "dispatchPreview.php",
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         async: false,
         success: function(response) {
         
         popupWindow = window.open("",'popUpWindow','height=3000,width=4000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
    popupWindow.document.write(response);

         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });
      }
      $('Creator').remove();
      var func = '<div class="col-sm-2 pull-right"><input type="button" name="register" class="btn btn-primary btn-user btn-block" onclick="mailPdf()" value="mail"></div><div class="col-sm-2 pull-right"><input type="button" name="register" class="btn btn-primary btn-user btn-block" onclick="printFunction()" value="download"></div>';
      $('functionalities').append(func);
    
  }



  function mailer(){
        var formPdf = new FormData();
       var htmlCode;
        var reciever = document.getElementById('mailId');
        if(reciever.value!=undefined){
        var tempOrder = JSON.stringify(orders);
        var formData = new FormData();
        formData.append("order",tempOrder);
        formData.append("reference",ref);
        $.ajax({
         url: "dispatchTablePreview.php",
         type: "POST",
         data: formData,
         processData: false,
         contentType: false,
         async: false,
         success: function(response) {
         
         htmlCode=response;

         },
         error: function(jqXHR, textStatus, errorMessage) {
             console.log(errorMessage); // Optional
         }
      });

          
      var formPdf1 = new FormData();
        formPdf1.append("html",htmlCode);
        formPdf1.append("reciever",reciever.value);
         $.ajax({
       url: "PHPMailer-master/sample.php",
       type: "POST",
       data: formPdf1,
       processData: false,
       contentType: false,
       async: false,
       success: function(response) {
          if(response.includes("Message has been sent")){
            alert("Mail have been sent");
          }

              },
       error: function(jqXHR, textStatus, errorMessage) {
        
           console.log(errorMessage); // Optional
       }
     });
      }
      else{
        alert("Please enter mail id");
      }
    }
      
      function mailPdf(){
        
         
       if(orders!=undefined){
        $('rags').remove();
        $('functionalities').remove();
        var mailInput="<div class='row'><div class='col-sm-4'><input type='text'   class='form-control' id='mailId'/></div><div class='col-sm-4'><input type='button'   class='btn btn-primary btn-sm-4' onclick='mailer()' value='Submit'/></div></div></div>";
        $('mailer').append(mailInput);
       
      }
      else{
        alert("Please create one order");
      }
    }
</script>
  </body>
</html>
